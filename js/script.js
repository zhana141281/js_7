
function showList(array) {
    let list = array.map(function (elem) {
           return `<li>${elem}</li>`;
   });
    let newList = document.getElementById('getlist');
    newList.innerHTML = list.join('');
}


let startArray = ['Hello', 'World!', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

showList(startArray);

